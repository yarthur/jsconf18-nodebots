var tessel = require("tessel-io");
var five = require("johnny-five");

var board = new five.Board({
	io: new tessel
});

board.on("ready", function () {
	var led = new five.Led('a5');
	led.pulse(500);
})
