var tessel = require("tessel-io");
var five = require("johnny-five");

var board = new five.Board({
	io: new tessel
});

board.on("ready", function () {
	var buttons = new five.Buttons(['a5', 'a6']),
		leds = new five.Leds(['b5', 'b6']);

	buttons.on('press', function (button) {
		leds.off();
		leds[buttons.indexOf(button)].on();
	});
});
