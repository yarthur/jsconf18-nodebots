var tessel = require("tessel-io");
var five = require("johnny-five");

var board = new five.Board({
	io: new tessel
});

board.on("ready", function () {
	var leds = new five.Leds(['a2', 'a3', 'a4', 'a5', 'a6', 'a7']);

	var lc1 = 0,
		step = 1;

	board.loop(100, function () {
		leds.off();

		leds[lc1].on();
		leds[leds.length - lc1 - 1].on();

		lc1 += step;

		if (lc1 === 0 || lc1 === leds.length -1) {
			step *= -1;
		}
	});
});
