var tessel = require("tessel-io");
var five = require("johnny-five");

var board = new five.Board({
	io: new tessel
});

board.on("ready", function () {
	var sensor = five.Sensor({
		pin: "a7",
		threshold: 2
	});

	var led = five.Led("b5");

	sensor.on("change", function () {
		led.brightness(sensor.scaleTo(0, 255));
	});
});
